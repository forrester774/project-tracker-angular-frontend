import {Component, OnInit} from '@angular/core';
import {Project} from '../models/project';
import {ProjectService} from '../project.service';
import {MatSnackBar} from '@angular/material';
import { errorSnackBar, deleteSnackBar } from '../util/snackbar-helper';

@Component({
  selector: 'app-project-home',
  templateUrl: './project-home.component.html',
  styleUrls: ['./project-home.component.css']
})
export class ProjectHomeComponent implements OnInit {

  projects: Array<Project> = [];
  searchText: string;

  constructor(private projectService: ProjectService, private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.getProjects();
  }

  getProjects() {
    this.projectService.getAllProjects().subscribe(
      response => {
        this.projects = response;
      },
      error => {
        errorSnackBar(this.snackBar);
      }
    );
  }

  deleteProject(id: string) {
    this.projectService.delete(id).subscribe(
      response => {
        this.projects = response;
        deleteSnackBar(this.snackBar);
      },
      error => {
        errorSnackBar(this.snackBar);
      }
    );
  }
}
