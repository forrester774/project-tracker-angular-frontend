import { Component, Input, OnChanges, ChangeDetectorRef, Output } from '@angular/core';
import { Project } from '../models/project';
import { Task } from '../models/task';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ProjectService } from '../project.service';
import { ProjectTasksComponent } from '../project-tasks/project-tasks.component';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent extends ProjectTasksComponent {

  @Input() project: Project;

  @Output() projectChange: EventEmitter<Project> = new EventEmitter();

  constructor(projectService: ProjectService, snackBar: MatSnackBar, route: ActivatedRoute) {
    super(projectService, snackBar, route);
   }

   deleteTask(task: Task) {
    this.project.tasks.splice(this.project.tasks.indexOf(task), 1);
    this.projectChange.emit(this.project);
  }

  drop(event: CdkDragDrop<Array<Task>>) {
    moveItemInArray(this.project.tasks, event.previousIndex, event.currentIndex);
    this.updateProject(this.project);
  }
}
