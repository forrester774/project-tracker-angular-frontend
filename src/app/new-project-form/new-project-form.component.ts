import {Component, ViewChild} from '@angular/core';
import {Project} from '../models/project';
import {ProjectService} from '../project.service';
import {MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { newProjectSnackBar, errorSnackBar } from '../util/snackbar-helper';


@Component({
  selector: 'app-new-project-form',
  templateUrl: './new-project-form.component.html',
  styleUrls: ['./new-project-form.component.css']
})
export class NewProjectFormComponent {

  project: Project = new Project();

  @ViewChild('form') form: FormGroup;

  constructor(private projectService: ProjectService, public snackBar: MatSnackBar, private router: Router) {
  }

  onSubmit() {
    if (this.form.valid) {
      this.projectService.createProject({...this.form.value, tasks: []} as Project).subscribe(
        res => [
          newProjectSnackBar(this.snackBar),
        this.router.navigateByUrl('projects')
      ],
      error => {
        errorSnackBar(this.snackBar);
      }
      );
    }
  }
}
