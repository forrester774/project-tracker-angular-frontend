import { Component, OnInit, ViewChild, ChangeDetectorRef, OnChanges } from '@angular/core';
import {ProjectService} from '../project.service';
import {Task} from '../models/task';
import {MatSnackBar} from '@angular/material';
import { Project } from '../models/project';
import { ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { errorSnackBar, taskUpdatedSnackBar } from '../util/snackbar-helper';

@Component({
  selector: 'app-project-tasks',
  templateUrl: './project-tasks.component.html',
  styleUrls: ['./project-tasks.component.css']
})
export class ProjectTasksComponent implements OnInit {

  project: Project;
  @ViewChild('form') form: FormGroup;
  newTask: boolean;
  task: Task;

  constructor(private projectService: ProjectService, private snackBar: MatSnackBar,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.getProject();
    this.newTask = false;
  }

  getProject() {
     this.projectService.getProjectById(this.route.snapshot.params['id']).subscribe(
       response => {
         this.project = response;
       },
       error => {
          errorSnackBar(this.snackBar);
       }
     );
  }

  setupNewTask() {
    this.task = new Task();
    this.newTask = !this.newTask;
  }

  onSubmit() {
    if (this.form.valid) {
      this.project.tasks.push(this.task);
      this.updateProject(this.project);
      this.newTask = false;
    }
  }

  updateProject(project: Project) {
    this.projectService.updateProject(project).subscribe(
      response => {
        this.project = response;
        taskUpdatedSnackBar(this.snackBar);
      },
      error => {
        errorSnackBar(this.snackBar);
      }
    );
  }
}
