import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material';
import {NavbarComponent} from './navbar/navbar.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {NewProjectFormComponent} from './new-project-form/new-project-form.component';
import {NotFoundComponent} from './not-found/not-found.component';
import { ProjectTasksComponent } from './project-tasks/project-tasks.component';
import { FilterProjectPipe } from './filter-project.pipe';
import { NewProjectSnackBarComponent } from './snackbars/new-project-snack-bar/new-project-snack-bar.component';
import { DeleteSnackbarComponent } from './snackbars/delete-snackbar/delete-snackbar.component';
import { ErrorSnackbarComponent } from './snackbars/error-snackbar/error-snackbar.component';
import { ProjectHomeComponent } from './project-home/project-home.component';
import { ProjectComponent } from './project/project.component';
import { TaskUpdatedSnackbarComponent } from './snackbars/task-updated-snackbar/task-updated-snackbar.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ProjectHomeComponent,
    NewProjectFormComponent,
    NotFoundComponent,
    ProjectTasksComponent,
    FilterProjectPipe,
    NewProjectSnackBarComponent,
    DeleteSnackbarComponent,
    ErrorSnackbarComponent,
    ProjectComponent,
    TaskUpdatedSnackbarComponent,
  ],
  entryComponents: [NewProjectSnackBarComponent,
    DeleteSnackbarComponent, ErrorSnackbarComponent, TaskUpdatedSnackbarComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
